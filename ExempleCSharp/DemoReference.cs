﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExempleCSharp
{
    class DemoReference
    {
        public void Tester()
        {
            int x;
            bool ok = false;
            do
            {
                Console.WriteLine("Veuillez saisir un nombre : ");

                string s = Console.ReadLine();
                // x = int.Parse(s);
                ok = int.TryParse(s, out x); // elle va tester s si ok elle va mettre le résultat dans x (en out car pas encore attribué)
            }
            while (!ok);

            bool resultat = Doubler(ref x);

            int p;

            Attribuer(out p);

            Console.WriteLine("Le double de x vaut : " + x);
        }

        public bool Doubler(ref int x)
        {
            x *= 2;
            return x < 100;
        }
        public void Attribuer(out int x)
        {
            x = 42;
        }
    }
}
