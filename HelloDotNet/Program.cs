﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            String val = "52";
            string cxv = "42";

            int x = int.Parse(val);
            Int32 y = Int32.Parse(val);

            long l = 42;
            Int64 ll = 455;

            bool b = true;
            Boolean c = false;

            // List<Departement> depts = new ArrayList<Departement>();
            IList<Departement> depts = new List<Departement>();

            depts.Add(new Departement("Creuse", "23"));
            depts.Add(new Departement("Val de Marne", "94"));
            depts.Add(new Departement("Finistère", "29"));

            // int nbElements = depts.size()
            int nbElements = depts.Count; // property (get)

            // Departement d = depts.getItem(2)
            Departement d = depts[2]; // indexeur

            depts.Clear(); // je vide ma liste

            Camion cam = new Camion();

            cam.Puissance = 1264645;

            cam.Beaute = -100;

            Ville v = new Ville();
            v.Superficie = 105.4;
            v.Population = 2000000;
            v.Departement = new Departement("Dordogne", "24");

            Console.WriteLine(v.Densite);
            Console.WriteLine(v.Departement.Nom);

            Console.WriteLine("Hello .NET !!");

            Avion a = new Avion();

            a.Envergure *= 2;

            Console.WriteLine(a.Envergure);
            a.Envergure = 2;

            Console.ReadLine();
        }
    }
}
