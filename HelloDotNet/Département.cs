﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Département
    {
        #region PROPERTIES
        public string Nom { get; set; }
        public string Numero { get; set; }
        #endregion

        #region CONSTRUCTOR
        public Département(string Nom, string Numero)
        {
            this.Nom = Nom;
            this.Numero = Numero;
        }
        #endregion
    }
}
