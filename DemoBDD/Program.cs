﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace DemoBDD
{
    class Program
    {
        static void Main(string[] args)
        {
            //demo1();
            Console.WriteLine(GetTailleMoyenne());

            Console.ReadLine();

        }

        public static float GetTailleMoyenne()
        {
            // 1 - configurer une connection
            IDbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = "Server=localhost;Database=goelands;Uid=root;Pwd=root;";

            // 2 - préparer une commande SQL
            IDbCommand cmd = new MySqlCommand();

            cmd.CommandText = "SELECT avg(taille) FROM adherent";
            cmd.Connection = cnx;  // permet d'informer la commande sur la connexion à utliser
            float resultat = 0;
            try
            {
                // 3 - ouvrir la connexion
                cnx.Open();

                // 4 - exécuter la commande
                resultat = Convert.ToSingle(cmd.ExecuteScalar());

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - fermer la connexion
                cnx.Close();
            }
            return resultat;
        }

        public static void demo1()
        { 
            // 1 - configurer une connection
            IDbConnection cnx = new MySqlConnection();
           //string host = "localhost";
           // string port = "3306"
           // string user = "root";
           // string password = "root";
           // string dbname = "goelands";

            cnx.ConnectionString = "Server=localhost;Database=goelands;Uid=root;Pwd=root;";

            // 2 - préparer une commande SQL
            IDbCommand cmd = new MySqlCommand();

            cmd.CommandText = "SELECT * FROM adherent";
            cmd.Connection = cnx;  // permet d'informer la commande sur la connexion à utliser

            try
            {
                // 3 - ouvrir la connexion
                cnx.Open();

                // 4 - exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - traiter le résultat
                while(dr.Read())
                {
                    //afficher le nom
                    int id = dr.GetInt32(dr.GetOrdinal("id"));
                    string nom = dr.GetString(dr.GetOrdinal("nom"));
                    DateTime dateNaissance = dr.GetDateTime(dr.GetOrdinal("date de naissance"));
                    Console.WriteLine(id + " : " + nom + " né(e) le : " + dateNaissance);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - fermer la connexion
                cnx.Close();
            }
        }
    }
}
