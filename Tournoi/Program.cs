﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.AFCEPF.IntroCSharp.MesOutils;

namespace Tournoi
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1 - ajouter l'assembly à utiliser
            // 2 - verifier que la classe à utiliser est publique
            // 3 - ajouter une directive using sur le namespace choisi

            int annee = 1492;
            Calendrier c = new Calendrier();

            String message = string.Format("L'année {0} {1} bissextile", 
                               annee, 
                               (c.EstBissextile(annee)) ? "est" : "n'est pas"
                             );
            Console.WriteLine(message);
            Console.ReadLine();
        }
    }
}
